<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class HelloWorldPackage extends Package {

     protected $pkgHandle = 'hello_world';
     protected $appVersionRequired = '5.3.0';
     protected $pkgVersion = '1.0';

     public function getPackageDescription() {
          return t("Just an example package that says Hello World.");
     }

     public function getPackageName() {
          return t("Hello World: The Packaging");
     }
     
     public function install() {
          $pkg = parent::install();
     
          // install block 
          BlockType::installBlockTypeFromPackage('hello_world', $pkg); 
     }
     
}
