<?php defined('C5_EXECUTE') or die(_("Access Denied."));
	
class HelloWorldBlockController extends BlockController {
	
	protected $btTable = "btHelloWorld";//optional, but usually required
	protected $btInterfaceWidth = "350";
	protected $btInterfaceHeight = "300";
	protected $btDescription = 'Just a sample block';//required
	protected $btName = 'Hello, World';//required

/*
	public function getBlockTypeName() {
		return t('Hello World');
	}

	public function getBlockTypeDescription() {
		return t('A simple testing block for developers');
	}
*/
	
}
