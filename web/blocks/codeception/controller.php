<?php defined('C5_EXECUTE') or die(_("Access Denied."));
	
class CodeceptionBlockController extends BlockController {
	
	protected $btTable = "btcodeception";//optional, but usually required
	protected $btInterfaceWidth = "350";
	protected $btInterfaceHeight = "300";
	protected $btDescription = 'Just a sample block';//required
	protected $btName = 'Codeception: Initial tests (#2) --> include codecept.phar file';//required
	
}
