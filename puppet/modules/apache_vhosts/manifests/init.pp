# == Class: apache_vhosts
#
# Adds and enables virtual hosts. Sets up /var/www symlinks.
#
class apache_vhosts {
	file { 
		'/var/www':
		ensure => directory;
	}


	# Can add many vhosts to reference conf file in the array.
	apache_vhosts::vhost { [$sitedomain]: }
}
