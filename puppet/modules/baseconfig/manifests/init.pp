# == Class: baseconfig
#
# Performs initial configuration tasks for all Vagrant boxes.
#
class baseconfig {
	exec { 'apt-get update':
		command => '/usr/bin/apt-get update';
	}

	package { ['vim', 'git', 'nfs-common']:
		ensure => present;
		#require => Exec['apt-get update'];
	}

	file { '/home/vagrant/.bashrc':
		owner => 'vagrant',
		group => 'vagrant',
		mode  => '0644',
		source => 'puppet:///modules/baseconfig/bashrc';
	}
}