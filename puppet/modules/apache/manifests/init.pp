# == Class: apache
#
# Installs packages for Apache2, enables modules, and sets config files.
#
class apache {

	package { ['apache2', 'apache2-mpm-prefork']:
		ensure => present,
	}

	# Notify this when apache needs a reload. This is only needed when
	# sites are added or removed, since a full restart then would be
	# a waste of time. When the module-config changes, a force-reload is
	# needed.
	exec { "reload-apache2":
		command => "/etc/init.d/apache2 reload",
		refreshonly => true,
	}

	exec { "force-reload-apache2":
		command => "/etc/init.d/apache2 force-reload",
		refreshonly => true,
	}

	# We want to make sure that Apache2 is running.
	service { "apache2":
		ensure => running,
		hasstatus => true,
		hasrestart => true,
		require => Package["apache2"],
	}

	exec { 'disable-default-site':
		command => "/usr/sbin/a2dissite default",
		notify => Exec["force-reload-apache2"],
		require => Service['apache2'],
	}

	apache::conf { ['apache2.conf', 'envvars', 'ports.conf']: }
	apache::module { ['expires.load', 'headers.load', 'proxy.conf', 'proxy.load', 'proxy_http.load', 'rewrite.load', 'deflate.load', 'ssl.conf', 'ssl.load']: }
}
