define mysql::db_create( $user, $password ) {
	exec { "create-${name}-db":
		unless => "/usr/bin/mysql -u${user} -p${password} ${name}",
		command => "/usr/bin/mysql -uroot -proot -e 'create database `${name}`';",
		require => [Service["mysql"], Exec["set-mysql-password"]]
	}

	exec { "grant-${name}-db":
		command => "/usr/bin/mysql -uroot -proot -e 'grant all on `${name}`.* to ${user}@localhost identified by \"${password}\";'",
		require => [Service["mysql"], Exec["create-${name}-db"]]
	}

	exec { "load-${name}-sql":
		command => "/usr/bin/mysql -u ${user} -p${password} ${name} < /vagrant/production.sql",
		require => Exec["grant-${name}-db"]
	}
}